function getBooks() {
    let books = [];
    // delay 1 second (1000ms)
    setTimeout(() => {
      books = [
        { title: 'Designing Data Intensive Applications', author: 'Martin Kleppmann' },
        { title: 'The Psychology of Money', author: 'Morgan Housel' },
      ];
    }, 1000);
  
    return books;
}

function findAuthor(bookTitle) {
    const books = getBooks();
    const book = books.find((book) => book.title === bookTitle); // B
    return book;
}

console.log(findAuthor('The Psychology of Money'))