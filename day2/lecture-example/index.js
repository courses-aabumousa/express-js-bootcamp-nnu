import express from 'express';

// create express instance
const app = express();
// extract json data
app.use(express.json());

app.post('/calculate', (req, res, next) => {
    const { x ,y ,operation } = req.body;
    if (typeof x !== 'number' || typeof y !== 'number' ||  !['+','-','*','/'].includes(operation)) {
        throw new Error();
    }
    next()
}, (req, res) => {
    const { x ,y ,operation } = req.body;
    let result;
    switch (operation) {
        case '+' :
            result = x + y;
            break;
        case '-': 
            result = x - y;
            break;
        case '*':
            result = x * y;
            break;
        case '/':
            result = x / y;
            break;
    }
    res.send({ result })
})

app.listen(3000, () =>
  console.log(`🚀 Server ready at: http://localhost:3000`)
);
